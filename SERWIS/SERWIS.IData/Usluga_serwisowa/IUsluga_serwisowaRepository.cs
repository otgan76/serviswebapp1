﻿using System;
using System.Threading.Tasks;


namespace SERWIS.IData.Usluga_serwisowa
{
    public interface IUsluga_serwisowaRepository
    {
        Task<int> AddUsluga_serwisowa(SERWIS.Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa);
        Task<SERWIS.Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByID(int Usluga_serwisowa_ID);
        Task<SERWIS.Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByClientID(int Klient_ID);
        Task EditUsluga_serwisowa(Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa);
        Task DelUslugaByID(Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa);
    }
}
