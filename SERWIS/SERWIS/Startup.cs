using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SERWIS.Data.Sql;
using SERWIS.Data.Sql.Migrations;
using SERWIS.Data.Sql.Usluga_serwisowa;
using SERWIS.IData.Usluga_serwisowa;
using SERWIS.Api.BindingModels;
using SERWIS.Api.Middlewares;
using SERWIS.Api.Validation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using FluentValidation;
using FluentValidation.AspNetCore;
using SERWIS.IServices.Usluga_serwisowa;
using SERWIS.Services.Usluga_serwisowa;

namespace SERWIS
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SERWISDbContext>(options => options
                .UseMySQL(Configuration.GetConnectionString("SERWISDbContext")));
            services.AddTransient<DatabaseSeed>();
            services.AddControllers().AddNewtonsoftJson(options =>
            {
                options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            })
            .AddFluentValidation();
            services.AddTransient<IValidator<EditUsluga_serwisowa>, EditUsluga_serwisowaValidator>();
            services.AddTransient<IValidator<CreateUsluga_serwisowa>, CreateUsluga_serwisowaValidator>();
            services.AddScoped<IUsluga_serwisowaService, Usluga_serwisowaService>();
            services.AddScoped<IUsluga_serwisowaRepository, Usluga_serwisowaRepository>();
            

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.UseApiBehavior = false;
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<SERWISDbContext>();
                var databaseSeed = serviceScope.ServiceProvider.GetRequiredService<DatabaseSeed>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();
                databaseSeed.Seed();
            }

            app.UseMiddleware<ErrorhandlerMiddleware>();
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });



        }
    }
}
