﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using SERWIS.Api.BindingModels;

namespace SERWIS.Api.Validation
{
    public class CreateUsluga_serwisowaValidator : AbstractValidator<CreateUsluga_serwisowa>
    {
        public CreateUsluga_serwisowaValidator()
        {
            RuleFor(x => x.Usluga_serwisowa_ID).NotNull();
            RuleFor(x => x.ID_pracownika).NotNull();
            RuleFor(x => x.Klient_ID).NotNull();
            RuleFor(x => x.Cena_uslugi).NotNull();
            RuleFor(x => x.Data_zakonczenia).NotNull();
        }
    }
}
