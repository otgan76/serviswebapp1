﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using SERWIS.Domain.DomainExceptions;

namespace SERWIS.Api.Middlewares
{
    public class ErrorhandlerMiddleware
    {
        private readonly RequestDelegate _next;
        
        public ErrorhandlerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                //var exp = exception.GetBaseException();
                //exp.GetType();
                await HandlerErrorAsync(context, exception);
            }
        }


        private Task HandlerErrorAsync(HttpContext context, Exception exception)
        {
            var exceptionResult = GetException(exception);
            var response = new { message = exceptionResult.Item1 };
            var payload = JsonConvert.SerializeObject(response);
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exceptionResult.Item2;

            return context.Response.WriteAsync(payload);
        }

        private (string, int) GetException(Exception exception)
        {
            var key = _exceptionDictionary.Keys.FirstOrDefault(x => x(exception));
            if (key == null)
            {
                return ("Internal Server Error", 500);
            }

            return _exceptionDictionary[key](exception);
        }

        private Dictionary<Func<Exception, bool>, Func<Exception, (string, int)>> _exceptionDictionary =
            new Dictionary<Func<Exception, bool>, Func<Exception, (string, int)>> 
            {
                [exception => exception.GetBaseException() is InvalidData_rozpoczeciaException]
                    = exception => (exception.GetBaseException().Message, 400)
            };


    }
}
