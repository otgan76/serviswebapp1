﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SERWIS.Api.ViewModels;


namespace SERWIS.Api.Mappers
{
    public class Usluga_serwisowaToUsluga_serwisowaViewModelMapper
    {
        public static Usluga_SerwisowaViewModel Usluga_serwisowaToUsluga_serwisowaViewModel(Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa)
        {
            var usluga_serwisowaViewModel = new Usluga_SerwisowaViewModel
            {
                Usluga_serwisowa_ID = usluga_serwisowa.Usluga_serwisowa_ID,
                Data_rozpoczecia = usluga_serwisowa.Data_rozpoczecia,
                Data_Zakonczenia = usluga_serwisowa.Data_zakonczenia,
                ID_pracownika = usluga_serwisowa.ID_pracownika,
                Klient_ID = usluga_serwisowa.Klient_ID,
                Cena_uslugi = usluga_serwisowa.Cena_uslugi,
                ID_ocena_uslugi = usluga_serwisowa.ID_ocena_uslugi
            };
            return usluga_serwisowaViewModel;
        }
    }
}
