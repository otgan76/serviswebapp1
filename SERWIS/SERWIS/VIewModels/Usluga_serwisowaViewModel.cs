﻿using System;

namespace SERWIS.Api.ViewModels
{
    public class Usluga_SerwisowaViewModel
    {


        public int Usluga_serwisowa_ID { get; set; }
        public DateTime Data_rozpoczecia { get; set; }
        public DateTime Data_Zakonczenia { get; set; }
        public int ID_pracownika { get; set; }
        public int Klient_ID { get; set; }
        public int Cena_uslugi { get; set; }
        public int ID_ocena_uslugi { get; set; }
        public int Rodzaj_uslugi_ID  { get; set; }
    }
}
