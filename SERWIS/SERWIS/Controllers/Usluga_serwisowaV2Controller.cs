﻿using System.Threading.Tasks;
using SERWIS.Api.Mappers;
using SERWIS.Api.Validation;
using SERWIS.Data.Sql;
using SERWIS.IServices.Usluga_serwisowa;
using Microsoft.AspNetCore.Mvc;

namespace SERWIS.Api.Controllers
{
  
        [ApiVersion("2.0")]
        [Route("api/v{version:apiVersion}/usluga_serwisowa")]
        public class Usluga_serwisowaV2Controller : Controller
        {
            private readonly SERWISDbContext _context;
            private readonly IUsluga_serwisowaService _usluga_serwisowaService;

            public Usluga_serwisowaV2Controller(SERWISDbContext context, IUsluga_serwisowaService usluga_serwisowaService)
            {
                _context = context; //dependency Injection. Przysyła obiekt
                _usluga_serwisowaService = usluga_serwisowaService;
            }

            [HttpGet("{Usluga_serwisowa_ID:min(1)}", Name = "GetUslugaByID")]
            public async Task<IActionResult> GetUslugaByID(int Usluga_serwisowa_ID)
            {
                var Usluga_serwisowa = await _usluga_serwisowaService.GetUslugaByID(Usluga_serwisowa_ID);

                if (Usluga_serwisowa != null)
                {
                    return Ok(Usluga_serwisowaToUsluga_serwisowaViewModelMapper.Usluga_serwisowaToUsluga_serwisowaViewModel(Usluga_serwisowa));
                }
                return NotFound();
            }
               


            [HttpGet("klientID/{Klient_ID}", Name = "GetUslugaByKlientID")]
            public async Task<IActionResult> GetUslugaByClientID(int Klient_ID)
            {
                var Usluga_serwisowa = await _usluga_serwisowaService.GetUslugaByClientID(Klient_ID);

                if (Usluga_serwisowa != null)
                {
                    return Ok(Usluga_serwisowaToUsluga_serwisowaViewModelMapper.Usluga_serwisowaToUsluga_serwisowaViewModel(Usluga_serwisowa));
                }
                return NotFound();
            }


            [ValidateModel]

            public async Task<IActionResult> Post([FromBody] IServices.Requests.CreateUsluga_serwisowa CreateUsluga_serwisowa)
            {
                var Usluga_serwisowa = await _usluga_serwisowaService.CreateUsluga_serwisowa(CreateUsluga_serwisowa);

                return Created(Usluga_serwisowa.Usluga_serwisowa_ID.ToString(), Usluga_serwisowaToUsluga_serwisowaViewModelMapper.Usluga_serwisowaToUsluga_serwisowaViewModel(Usluga_serwisowa));
     
            }


            [ValidateModel]
            [HttpPatch("edit/{Usluga_serwisowa_ID:min(1)}", Name = "EditUsluga_serwisowa")]

            public async Task<IActionResult> EditUsluga_serwisowa([FromBody] IServices.Requests.EditUsluga_serwisowa EditUsluga_serwisowa, int Usluga_serwisowa_ID)
            {
            await _usluga_serwisowaService.EditUsluga_serwisowa(EditUsluga_serwisowa, Usluga_serwisowa_ID);
            return NoContent();
            }

        // to do delete





            [HttpGet("del/{Usluga_serwisowa_ID:min(1)}", Name = "DelUslugaByID")]
            public async Task<IActionResult> DelUslugaByID([FromBody] IServices.Requests.DelUslugaByID DelUslugaByID, int Usluga_serwisowa_ID)
            {
                await _usluga_serwisowaService.DelUslugaByID(DelUslugaByID, Usluga_serwisowa_ID);
                return NoContent();
            }


        





















    }
}
