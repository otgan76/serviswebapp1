﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SERWIS.Data.Sql;
using SERWIS.Data.Sql.DAO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SERWIS.Api.ViewModels;
using SERWIS.Api.BindingModels;
using SERWIS.Api.Validation;

namespace SERWIS.Api.Controllers
{
    [ApiVersion( "1.0" )]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class Usluga_serwisowaController : Controller
    {
        private readonly SERWISDbContext _context;

        public Usluga_serwisowaController(SERWISDbContext context)
        {
            _context = context; //dependency Injection. Przysyła obiekt
        }

        [HttpGet("{Usluga_serwisowa_ID:min(1)}", Name = "GetUslugaByID")]
        public async Task<IActionResult> GetUslugaByID(int Usluga_serwisowa_ID)
        {
            var Usluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Usluga_serwisowa_ID == Usluga_serwisowa_ID);

            if (Usluga_serwisowa != null)
            {
                return Ok(new Usluga_SerwisowaViewModel
                {
                    Usluga_serwisowa_ID = Usluga_serwisowa.Usluga_serwisowa_ID,
                    Data_rozpoczecia = Usluga_serwisowa.Data_rozpoczecia,
                    Data_Zakonczenia = Usluga_serwisowa.Data_zakonczenia,
                    ID_pracownika = Usluga_serwisowa.ID_pracownika,
                    Klient_ID = Usluga_serwisowa.Klient_ID,
                    Cena_uslugi = Usluga_serwisowa.Cena_uslugi,
                    ID_ocena_uslugi = Usluga_serwisowa.ID_ocena_uslugi
                });
            }
            return NotFound();
        }

        [HttpGet("klientID/{Klient_ID}", Name = "GetUslugaByKlientID")]
        public async Task<IActionResult> GetUslugaByClientID(int Klient_ID)
        {
            var Usluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Klient_ID == Klient_ID);

            if (Usluga_serwisowa != null)
            {
                return Ok(new Usluga_SerwisowaViewModel
                {
                    Usluga_serwisowa_ID = Usluga_serwisowa.Usluga_serwisowa_ID,
                    Data_rozpoczecia = Usluga_serwisowa.Data_rozpoczecia,
                    Data_Zakonczenia = Usluga_serwisowa.Data_zakonczenia,
                    ID_pracownika = Usluga_serwisowa.ID_pracownika,
                    Klient_ID = Usluga_serwisowa.Klient_ID,
                    Cena_uslugi = Usluga_serwisowa.Cena_uslugi,
                    ID_ocena_uslugi = Usluga_serwisowa.ID_ocena_uslugi
                });
            }
            return NotFound();
        }


        [ValidateModel]
        //        [Consumes("application/x-www-form-urlencoded")]
        //        [HttpPost("create", Name = "CreateUser")]
        // FROM BODY pozwala odczytywać z ciała JSONA przy POST
        public async Task<IActionResult> Post([FromBody] CreateUsluga_serwisowa CreateUsluga_serwisowa)
        {
            var Usluga_serwisowa = new Usluga_serwisowa
            {
                Usluga_serwisowa_ID = CreateUsluga_serwisowa.Usluga_serwisowa_ID,
                ID_pracownika = CreateUsluga_serwisowa.ID_pracownika,
                Klient_ID = CreateUsluga_serwisowa.Klient_ID,
                Cena_uslugi = CreateUsluga_serwisowa.Cena_uslugi,
                Data_zakonczenia = CreateUsluga_serwisowa.Data_zakonczenia

            };
            await _context.AddAsync(Usluga_serwisowa);
            await _context.SaveChangesAsync();

            return Created(Usluga_serwisowa.Usluga_serwisowa_ID.ToString(), new Usluga_SerwisowaViewModel
            {
                Usluga_serwisowa_ID = Usluga_serwisowa.Usluga_serwisowa_ID,
                Data_rozpoczecia = Usluga_serwisowa.Data_rozpoczecia,
                Data_Zakonczenia = Usluga_serwisowa.Data_zakonczenia,
                ID_pracownika = Usluga_serwisowa.ID_pracownika,
                Klient_ID = Usluga_serwisowa.Klient_ID,
                Cena_uslugi = Usluga_serwisowa.Cena_uslugi,
                ID_ocena_uslugi = Usluga_serwisowa.ID_ocena_uslugi
            });
        }

        [ValidateModel]
        [HttpPatch("edit/{Usluga_serwisowa_ID:min(1)}", Name = "EditUsluga_serwisowa")]
        //        public async Task<IActionResult> EditUser([FromBody] EditUser editUser,[FromQuery] int userId)
        public async Task<IActionResult> EditUsluga_serwisowa([FromBody] EditUsluga_serwisowa EditUsluga_serwisowa, int Usluga_serwisowa_ID)
        {
            var Usluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Usluga_serwisowa_ID == Usluga_serwisowa_ID);

            Usluga_serwisowa.Usluga_serwisowa_ID = EditUsluga_serwisowa.Usluga_serwisowa_ID;
            Usluga_serwisowa.ID_pracownika = EditUsluga_serwisowa.ID_pracownika;
            Usluga_serwisowa.Klient_ID = EditUsluga_serwisowa.Klient_ID;
            Usluga_serwisowa.Cena_uslugi = EditUsluga_serwisowa.Cena_uslugi;
            Usluga_serwisowa.Data_zakonczenia = EditUsluga_serwisowa.Data_zakonczenia;
            await _context.SaveChangesAsync();
            return NoContent();
   
        }



        [HttpDelete("del/{Usluga_serwisowa_ID:min(1)}", Name = "DelUslugaByID")]
        public async Task<IActionResult> DelUslugaByID(int Usluga_serwisowa_ID)
        {
            var Usluga_serwisowa2 = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Usluga_serwisowa_ID == Usluga_serwisowa_ID);

            if (Usluga_serwisowa2 != null)
            {
                _context.Usluga_serwisowa.Remove(Usluga_serwisowa2);
                await _context.SaveChangesAsync();
                return NoContent();
            }
            return NotFound();
        }







    }
}
