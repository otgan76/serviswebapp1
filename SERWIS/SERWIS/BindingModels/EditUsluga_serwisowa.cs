﻿using System;
using System.ComponentModel.DataAnnotations;
using FluentValidation;

namespace SERWIS.Api.BindingModels
{
    public class EditUsluga_serwisowa
    {
        [Display(Name = "ID Usluga Serwisowa")]
        public int Usluga_serwisowa_ID { get; set; }

        [Display(Name = "ID Pracownika")]
        public int ID_pracownika { get; set; }

        [Display(Name = "ID Klienta")]
        public int Klient_ID { get; set; }

        [Display(Name = "Cena uslugi")]
        public int Cena_uslugi { get; set; }

        [Display(Name = "Data zakonczenia")]
        public DateTime Data_zakonczenia { get; set; }
    }



  

}
