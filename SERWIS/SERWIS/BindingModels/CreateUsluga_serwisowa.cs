﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SERWIS.Api.BindingModels
{
    public class CreateUsluga_serwisowa
    {

        [Required] //walidacja z klasy Validate na podstawie tych propercji
        [Display(Name = "ID Usluga Serwisowa")]
        public int Usluga_serwisowa_ID { get; set; }

        [Required]
        [Display(Name = "ID Pracownika")]
        public int ID_pracownika { get; set; }

        [Required]
        [Display(Name = "ID Klienta")]
        public int Klient_ID { get; set; }

        [Required]
        [Display(Name = "Cena uslugi")]
        public int Cena_uslugi { get; set; }

        [Required]
        [Display(Name = "Data zakonczenia")]
        public DateTime Data_zakonczenia { get; set; }
    }
}
