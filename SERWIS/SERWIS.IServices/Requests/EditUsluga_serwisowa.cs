﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERWIS.IServices.Requests
{
    public class EditUsluga_serwisowa
    {
        public int Usluga_serwisowa_ID { get; set; }
        public int ID_pracownika { get; set; }
        public int Klient_ID { get; set; }
        public int Cena_uslugi { get; set; }
        public DateTime Data_zakonczenia { get; set; }
    }
}
