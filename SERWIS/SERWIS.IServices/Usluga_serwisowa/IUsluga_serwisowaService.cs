﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERWIS.IServices.Requests;


namespace SERWIS.IServices.Usluga_serwisowa
{
    public interface IUsluga_serwisowaService
    {
        Task<SERWIS.Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByID(int Usluga_serwisowa_ID);
        Task<SERWIS.Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByClientID(int Klient_ID);
        Task<SERWIS.Domain.Usluga_serwisowa.Usluga_serwisowa> CreateUsluga_serwisowa(CreateUsluga_serwisowa createUsluga_serwisowa);
        Task EditUsluga_serwisowa(EditUsluga_serwisowa createUsluga_serwisowa, int Usluga_serwisowa_ID);
        Task DelUslugaByID(DelUslugaByID createUsluga_serwisowa, int Usluga_serwisowa_ID);
    }
}
