﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using SERWIS.IData.Usluga_serwisowa;



namespace SERWIS.Data.Sql.Usluga_serwisowa
{
    public class Usluga_serwisowaRepository: IUsluga_serwisowaRepository
    {
        private readonly SERWISDbContext _context;

        public Usluga_serwisowaRepository(SERWISDbContext context)
        {
            _context = context;
        }



        public async Task<int> AddUsluga_serwisowa(Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa)
        {
            var usluga_serwisowaDAO = new DAO.Usluga_serwisowa
            {
                Usluga_serwisowa_ID = usluga_serwisowa.Usluga_serwisowa_ID,
                Data_rozpoczecia = usluga_serwisowa.Data_rozpoczecia,
                Data_zakonczenia = usluga_serwisowa.Data_zakonczenia,
                ID_pracownika = usluga_serwisowa.ID_pracownika,
                Klient_ID = usluga_serwisowa.Klient_ID,
                Cena_uslugi = usluga_serwisowa.Cena_uslugi,
                ID_ocena_uslugi = usluga_serwisowa.ID_ocena_uslugi
            };
            await _context.AddAsync(usluga_serwisowaDAO);
            await _context.SaveChangesAsync();
            return usluga_serwisowaDAO.Usluga_serwisowa_ID;
        }

        public async Task<Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByID(int Usluga_serwisowa_ID)
        {
            var usluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Usluga_serwisowa_ID == Usluga_serwisowa_ID);
            return new Domain.Usluga_serwisowa.Usluga_serwisowa(usluga_serwisowa.Usluga_serwisowa_ID,
                usluga_serwisowa.Data_rozpoczecia,
                usluga_serwisowa.Data_zakonczenia,
                usluga_serwisowa.ID_pracownika,
                usluga_serwisowa.Klient_ID,
                usluga_serwisowa.Cena_uslugi,
                usluga_serwisowa.ID_ocena_uslugi);
        }
        public async Task<Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByClientID(int Klient_ID)
        {
            var usluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Klient_ID == Klient_ID);
            return new Domain.Usluga_serwisowa.Usluga_serwisowa(usluga_serwisowa.Usluga_serwisowa_ID,
                usluga_serwisowa.Data_rozpoczecia,
                usluga_serwisowa.Data_zakonczenia,
                usluga_serwisowa.ID_pracownika,
                usluga_serwisowa.Klient_ID,
                usluga_serwisowa.Cena_uslugi,
                usluga_serwisowa.ID_ocena_uslugi);
        }




        public async Task EditUsluga_serwisowa(Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa)
        {
            var editUsluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Usluga_serwisowa_ID == usluga_serwisowa.Usluga_serwisowa_ID);
            editUsluga_serwisowa.Usluga_serwisowa_ID = usluga_serwisowa.Usluga_serwisowa_ID;
            editUsluga_serwisowa.Data_rozpoczecia = usluga_serwisowa.Data_rozpoczecia;
            editUsluga_serwisowa.Data_zakonczenia = usluga_serwisowa.Data_zakonczenia;
            editUsluga_serwisowa.ID_pracownika = usluga_serwisowa.ID_pracownika;
            editUsluga_serwisowa.Klient_ID = usluga_serwisowa.Klient_ID;
            editUsluga_serwisowa.Cena_uslugi = usluga_serwisowa.Cena_uslugi;
            editUsluga_serwisowa.ID_ocena_uslugi = usluga_serwisowa.ID_ocena_uslugi;
            await _context.SaveChangesAsync();
        }

        // Usuwanie 
        public async Task DelUslugaByID(Domain.Usluga_serwisowa.Usluga_serwisowa usluga_serwisowa)
        {
            var delUsluga_serwisowa = await _context.Usluga_serwisowa.FirstOrDefaultAsync(x => x.Usluga_serwisowa_ID == usluga_serwisowa.Usluga_serwisowa_ID);
            _context.Usluga_serwisowa.Remove(delUsluga_serwisowa);
            await _context.SaveChangesAsync();
        }



    }
}
