﻿using System;
using System.Collections.Generic;
using System.Linq;
using SERWIS.Data.Sql;
using SERWIS.Data.Sql.DAO;
using System.Text;

namespace SERWIS.Data.Sql.Migrations
{
    public class DatabaseSeed
    {
        private readonly SERWISDbContext _context;

        public DatabaseSeed(SERWISDbContext context)
        {
            _context = context;
        }


        public void Seed()
        {
            #region CreatePracownik
            var PracownikList = BuildPracownikList();
            _context.Pracownik.AddRange(PracownikList);
            _context.SaveChanges();
            #endregion

         

            #region CreateRodzaj_uslugi
            var Rodzaj_uslugiList = BuildRodzaj_uslugiList();
            _context.Rodzaj_uslugi.AddRange(Rodzaj_uslugiList);
            _context.SaveChanges();
            #endregion

            #region CreateKlient
            var KlientList = BuildKlientList();
            _context.Klient.AddRange(KlientList);
            _context.SaveChanges();
            #endregion

            #region CreateUsluga_serwisowa
            var Usluga_serwisowaList = BuildUsluga_serwisowaList();
            _context.Usluga_serwisowa.AddRange(Usluga_serwisowaList);
            _context.SaveChanges();
            #endregion



            #region CreateOcena_Uslugi
            var Ocena_uslugiList = BuildOcena_uslugiList();
            _context.Ocena_uslugi.AddRange(Ocena_uslugiList);
            _context.SaveChanges();
            #endregion

        }

        private IEnumerable<Pracownik> BuildPracownikList()
        {
            var PracownikList = new List<Pracownik>();
            var Pracownik1 = new Pracownik()
            {
                Pracownik_ID = 1,
                Imie = "Marcin",
                Nazwisko = "Zawadzki",
                Wyplata = 5555,
                Data_zatrudnienia = new DateTime(2021, 1, 1),
                stanowisko = "Kierownik",
            };
            PracownikList.Add(Pracownik1);

            var Pracownik2 = new Pracownik()
            {
                Pracownik_ID = 2,
                Imie = "Jurek",
                Nazwisko = "Delta",
                Wyplata = 2555,
                Data_zatrudnienia = new DateTime(2021, 3, 3),
                stanowisko = "Serwisant",
            };
            PracownikList.Add(Pracownik2);

            for (int i = 3; i <= 6; i++)
            {
                var Pracownik3 = new Pracownik()
                {
                    Pracownik_ID = i,
                    Imie = "Marcin" + i,
                    Nazwisko = "Zawadzki" + i,
                    Wyplata = 222 + i,
                    Data_zatrudnienia = new DateTime(2021, 1, 1),
                    stanowisko = "Serwisant",
                };
                PracownikList.Add(Pracownik3);
            }

            return PracownikList;
        }
        private IEnumerable<Rodzaj_uslugi> BuildRodzaj_uslugiList()
        {
            var Rodzaj_uslugiList = new List<Rodzaj_uslugi>();
            var Czyszczenie = new Rodzaj_uslugi()
            {
                Rodzaj_uslugi_ID = 1,
                Opis = "Czyszczenie Bla Bla Bla BlaBla Bla",
            };
            Rodzaj_uslugiList.Add(Czyszczenie);

            var Ulepszenie = new Rodzaj_uslugi()
            {
                Rodzaj_uslugi_ID = 2,
                Opis = "Ulepszenie Bla Bla Bla BlaBla Bla",
            };
            Rodzaj_uslugiList.Add(Ulepszenie);

            var Aktualizacja = new Rodzaj_uslugi()
            {
                Rodzaj_uslugi_ID = 3,
                Opis = "Aktualizacja Bla Bla Bla BlaBla Bla",
            };
            Rodzaj_uslugiList.Add(Aktualizacja);

            return Rodzaj_uslugiList;
        }
        private IEnumerable<Klient> BuildKlientList()
        {
            var KlientList = new List<Klient>();
            var Klient1 = new Klient()
            {
                Klient_ID = 1,
                Imie = "Bogdan",
                Nazwisko = "Lemba",
                Nr_Telefonu = 123444777,
                ID_ocena_uslugi = 1,
                Usluga_serwisowa_ID = 1,
            };
            KlientList.Add(Klient1);

            var Klient2 = new Klient()
            {
                Klient_ID = 2,
                Imie = "Bruno",
                Nazwisko = "Tesla",
                Nr_Telefonu = 123444777,
                ID_ocena_uslugi = 2,
                Usluga_serwisowa_ID = 3,
            };
            KlientList.Add(Klient1);

            for (int i = 3; i <= 6; i++)
            {
                var Klient3 = new Klient()
                {
                    Klient_ID = i,
                    Imie = "Adam" + i,
                    Nazwisko = "TOrczyk" + i,
                    Nr_Telefonu = 123444777 + i,
                    ID_ocena_uslugi = i,
                    Usluga_serwisowa_ID = i % 3,
                };
                KlientList.Add(Klient3);
            }

            return KlientList;
        }
        private IEnumerable<Ocena_uslugi> BuildOcena_uslugiList()
        {
            var Ocena_uslugiList = new List<Ocena_uslugi>();

            for (int i = 3; i <= 6; i++)
            {
                var Ocena_uslugi1 = new Ocena_uslugi()
                {
                    Ocena_uslugi_ID = i,
                    Ocena = i % 5,
                    Komentarz = "Komentarz nr " + i,
                    ID_klienta = i,
                };
            }
            return Ocena_uslugiList;  
    }
        private IEnumerable<DAO.Usluga_serwisowa> BuildUsluga_serwisowaList()
        {
            var Usluga_serwisowaList = new List<DAO.Usluga_serwisowa>();
            for (int i = 1; i <= 6; i++)
            {
                var UslugaSerwisowa1 = new DAO.Usluga_serwisowa()
                {
                    Usluga_serwisowa_ID = i+1,
                    Data_rozpoczecia = new DateTime(2021, 5 + i, 2 + (2 * i)),
                    Data_zakonczenia = new DateTime(2021, 5 + i, 7 + (2 * i)),
                    ID_pracownika = i,
                    Klient_ID = i,
                    Cena_uslugi = (i * 500 * i) / (i * 3),
                    ID_ocena_uslugi = i,
                };
                Usluga_serwisowaList.Add(UslugaSerwisowa1);
            }

            return Usluga_serwisowaList;
        }
    }
}

       
