﻿using SERWIS.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SERWIS.Data.SqlDAOConfiguration
{
   public class usluga_serwisowaConfiguration : IEntityTypeConfiguration<Usluga_serwisowa>
    {
        public void Configure(EntityTypeBuilder<Usluga_serwisowa> builder)
        {
            builder.Property(c => c.Usluga_serwisowa_ID);
            builder.Property(c => c.Data_rozpoczecia);
            builder.Property(c => c.Data_zakonczenia);
            builder.Property(c => c.ID_pracownika);
            builder.Property(c => c.Klient_ID);
            builder.Property(c => c.Cena_uslugi);
            builder.Property(c => c.ID_ocena_uslugi);

            //builder.HasOne(x => x.Rodzaj_uslugi)
            //.WithMany(x => x.Usluga_serwisowax)
            //.OnDelete(DeleteBehavior.Restrict)
            //.HasForeignKey(x => x.Usluga_serwisowa_ID);



            builder.HasOne(x => x.Pracownik)
                .WithMany(x => x.Usluga_serwisowax)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ID_pracownika);
        }
    }
}