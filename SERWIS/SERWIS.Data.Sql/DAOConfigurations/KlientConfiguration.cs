﻿using SERWIS.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SERWIS.Data.SqlDAOConfiguration
{
   public class KlientConfiguration : IEntityTypeConfiguration<Klient>
    {
        public void Configure(EntityTypeBuilder<Klient> builder)
        {
            builder.Property(c => c.Klient_ID);
            builder.Property(c => c.Imie);
            builder.Property(c => c.Nazwisko);
            builder.Property(c => c.Nr_Telefonu);
            builder.Property(c => c.ID_ocena_uslugi);
            builder.Property(c => c.Usluga_serwisowa_ID);
        }
    }
}