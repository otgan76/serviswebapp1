﻿using SERWIS.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SERWIS.Data.SqlDAOConfiguration
{
   public class Rodzaj_uslugiConfiguration : IEntityTypeConfiguration<Rodzaj_uslugi>
    {
        public void Configure(EntityTypeBuilder<Rodzaj_uslugi> builder)
        {
            builder.Property(c => c.Rodzaj_uslugi_ID);
            builder.Property(c => c.Opis);
        }
    }
}