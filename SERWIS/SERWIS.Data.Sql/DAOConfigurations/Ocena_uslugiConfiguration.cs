﻿using SERWIS.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SERWIS.Data.SqlDAOConfiguration
{
    public class Ocena_UslugiConfiguration : IEntityTypeConfiguration<Ocena_uslugi>
    {
        public void Configure(EntityTypeBuilder<Ocena_uslugi> builder)
        {
            builder.HasOne(x => x.Usluga_serwisowa)
                .WithMany(x => x.Ocena_uslugix)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.Ocena_uslugi_ID);
            builder.HasOne(x => x.Klient)
                .WithMany(x => x.Ocena_uslugiX)
                .OnDelete(DeleteBehavior.Restrict)
                .HasForeignKey(x => x.ID_klienta);
            builder.Property(c => c.Ocena_uslugi_ID);
            builder.Property(c => c.Ocena);
            builder.Property(c => c.Komentarz);
            builder.Property(c => c.ID_klienta);
    }
    }
}