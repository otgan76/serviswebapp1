using SERWIS.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace SERWIS.Data.SqlDAOConfiguration
{
    public class PracownikConfiguration: IEntityTypeConfiguration<Pracownik>
    {
        public void Configure(EntityTypeBuilder<Pracownik> builder)
        {
            builder.Property(c => c.Pracownik_ID).IsRequired();
        }
    }
}





 