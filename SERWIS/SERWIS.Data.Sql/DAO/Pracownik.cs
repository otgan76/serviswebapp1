using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace SERWIS.Data.Sql.DAO
{
    public class Pracownik
    {
        public Pracownik()
        {
            Usluga_serwisowax = new List<Usluga_serwisowa>();
        }

        [Key] public int Pracownik_ID {get; set;}
        public string Imie {get; set;}
        public string Nazwisko {get; set;}
        public int Wyplata {get; set;}
        public DateTime Data_zatrudnienia {get; set;}
        public string stanowisko {get; set;}

        public virtual ICollection<Usluga_serwisowa> Usluga_serwisowax {get; set;}
    }
}