using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SERWIS.Data.Sql.DAO
{
    public class Rodzaj_uslugi
    {
        public Rodzaj_uslugi()
        {
            Usluga_serwisowax = new List<Usluga_serwisowa>();
        }

        [Key] public int Rodzaj_uslugi_ID {get; set;}
        public string Opis {get; set;}
    
        public virtual ICollection<Usluga_serwisowa> Usluga_serwisowax {get; set;}
    }
}