using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations;

namespace SERWIS.Data.Sql.DAO
{
    public class Usluga_serwisowa
    {
        public Usluga_serwisowa()
        {
            Ocena_uslugix = new List<Ocena_uslugi>();
            Klientx = new List<Klient>();
        }

        [Key] public int Usluga_serwisowa_ID {get; set;}
        public DateTime Data_rozpoczecia {get; set;}
        public DateTime Data_zakonczenia {get; set;}
        public int ID_pracownika {get; set;}
        public int Klient_ID {get; set;}
        public int Cena_uslugi {get; set;}
        public int ID_ocena_uslugi {get; set;}
        
        public virtual ICollection<Ocena_uslugi> Ocena_uslugix {get; set;}
        public virtual ICollection<Klient> Klientx { get; set; }
        public virtual Pracownik Pracownik {get; set;}
        public virtual Rodzaj_uslugi Rodzaj_uslugi {get; set;}
        
    }
}