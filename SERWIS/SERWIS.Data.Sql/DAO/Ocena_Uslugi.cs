using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace SERWIS.Data.Sql.DAO
{
    public class Ocena_uslugi
    {

        [Key] public int Ocena_uslugi_ID{get; set;}
        public int Ocena {get; set;}
        public string Komentarz {get; set;}
        public int ID_klienta {get; set;}
    
        public virtual Usluga_serwisowa Usluga_serwisowa {get; set;}
        public virtual Klient Klient {get; set;}
    }
}