using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SERWIS.Data.Sql.DAO
{
    public class Klient
    {
        public Klient()
        {
            Ocena_uslugiX = new List<Ocena_uslugi>();
        }

        
        [Key] public int Klient_ID {get; set;}
        public string Imie {get; set;}
        public string Nazwisko {get; set;}
        public int Nr_Telefonu {get; set;}
        public int ID_ocena_uslugi {get; set;}
        public int Usluga_serwisowa_ID {get; set;}

    
        public virtual Usluga_serwisowa Usluga_serwisowa {get; set;}
        public virtual ICollection<Ocena_uslugi> Ocena_uslugiX {get; set;}
    }
}