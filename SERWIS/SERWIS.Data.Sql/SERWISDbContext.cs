﻿using SERWIS.Data.Sql.DAO;
using Microsoft.EntityFrameworkCore;
using SERWIS.Data.SqlDAOConfiguration;

namespace SERWIS.Data.Sql
{
    //Klasa odpowiadająca za konfigurację Entity Framework Core
    //Przy pomocy instancji klasy FoodlyDbContext możliwe jest wykonywanie
    //wszystkich operacji na bazie danych od tworzenia bazy danych po zapytanie do bazy danych itd.
    public class SERWISDbContext : DbContext
    {
        public SERWISDbContext(DbContextOptions<SERWISDbContext> options) : base(options) { }

        //Ustawienie klas z folderu DAO jako tabele bazy danych
        public virtual DbSet<Pracownik> Pracownik { get; set; }
        public virtual DbSet<DAO.Usluga_serwisowa> Usluga_serwisowa { get; set; }
        public virtual DbSet<Rodzaj_uslugi> Rodzaj_uslugi { get; set; }
        public virtual DbSet<Klient> Klient { get; set; }
        public virtual DbSet<Ocena_uslugi> Ocena_uslugi { get; set; }


        //Przykład konfiguracji modeli/encji poprzez klasy konfiguracyjne z folderu DAOConfigurations
        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new PracownikConfiguration());
            builder.ApplyConfiguration(new usluga_serwisowaConfiguration());
            builder.ApplyConfiguration(new Rodzaj_uslugiConfiguration());
            builder.ApplyConfiguration(new KlientConfiguration());
            builder.ApplyConfiguration(new Ocena_UslugiConfiguration());
        }

       

    }
}