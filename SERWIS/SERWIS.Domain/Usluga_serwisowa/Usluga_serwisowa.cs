﻿using System;
using SERWIS.Domain.DomainExceptions;

namespace SERWIS.Domain.Usluga_serwisowa
{
    public class Usluga_serwisowa
    {
        public int Usluga_serwisowa_ID { get; set; }
        public DateTime Data_rozpoczecia { get; private set; }
        public DateTime Data_zakonczenia { get; private set; }
        public int ID_pracownika { get; private set; }
        public int Klient_ID { get; private set; }
        public int Cena_uslugi { get; private set; }
        public int ID_ocena_uslugi { get; private set; }


        public Usluga_serwisowa(int usluga_serwisowa_id, 
            DateTime data_rozpoczecia,
            DateTime data_zakonczenia, 
            int id_pracownika,
            int klient_id,
            int cena_uslugi,
            int id_ocena_uslugi)
        {
            Usluga_serwisowa_ID = usluga_serwisowa_id;
            Data_rozpoczecia = data_rozpoczecia;
            Data_zakonczenia = data_zakonczenia;
            ID_pracownika = id_pracownika;
            Klient_ID = klient_id;
            Cena_uslugi = cena_uslugi;
            ID_ocena_uslugi = id_ocena_uslugi;
        }


        public Usluga_serwisowa(int usluga_serwisowa_id, 
            DateTime data_rozpoczecia,
            int id_pracownika,
            int klient_id,
            int cena_uslugi)
        {
            if (data_rozpoczecia < DateTime.UtcNow)
                throw new InvalidData_rozpoczeciaException(data_rozpoczecia);
            Usluga_serwisowa_ID = usluga_serwisowa_id;
            Data_rozpoczecia = data_rozpoczecia;
            DateTime Data_zakonczenia = new DateTime(1999, 10, 10);
            ID_pracownika = id_pracownika;
            Klient_ID = klient_id;
            Cena_uslugi = cena_uslugi;
            ID_ocena_uslugi = 0;
        }

        public void EditUsluga_serwisowa(int usluga_serwisowa_id, int id_pracownika, int cena_uslugi)
        {
            Usluga_serwisowa_ID = usluga_serwisowa_id;
            ID_pracownika = id_pracownika;
            Cena_uslugi = cena_uslugi;
        }

        public void DelUslugaByID(int usluga_serwisowa_id)
        {
            Usluga_serwisowa_ID = usluga_serwisowa_id;
        }




    }
}
