﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SERWIS.Domain.DomainExceptions
{
    public class InvalidData_rozpoczeciaException: Exception
    {
        public InvalidData_rozpoczeciaException(DateTime Data_rozpoczecia): base(ModifyMessage(Data_rozpoczecia))
        {
        }

        private static string ModifyMessage(DateTime Data_rozpoczecia)
        {
            return $"Nieprawidlowy data zakonczenia {Data_rozpoczecia}.";
        }
    }
}
