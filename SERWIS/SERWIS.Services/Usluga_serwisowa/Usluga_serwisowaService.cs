﻿using System;
using SERWIS.IData.Usluga_serwisowa;
using SERWIS.IServices.Requests;
using SERWIS.IServices.Usluga_serwisowa;
using System.Threading.Tasks;



namespace SERWIS.Services.Usluga_serwisowa
{
    public class Usluga_serwisowaService: IUsluga_serwisowaService
    {
        private readonly IUsluga_serwisowaRepository _usluga_serwisowaRepository;

        public Usluga_serwisowaService(IUsluga_serwisowaRepository usluga_SerwisowaRepository)
        {
            _usluga_serwisowaRepository = usluga_SerwisowaRepository;
        }

        public Task<Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByID(int Usluga_serwisowa_ID)
        {
            return _usluga_serwisowaRepository.GetUslugaByID(Usluga_serwisowa_ID);
        }

        public Task<Domain.Usluga_serwisowa.Usluga_serwisowa> GetUslugaByClientID(int Klient_ID)
        {
            return _usluga_serwisowaRepository.GetUslugaByClientID(Klient_ID);
        }

        public async Task<Domain.Usluga_serwisowa.Usluga_serwisowa> CreateUsluga_serwisowa(CreateUsluga_serwisowa createUsluga_serwisowa)
        {
            var usluga_serwisowa = new Domain.Usluga_serwisowa.Usluga_serwisowa(
                createUsluga_serwisowa.Usluga_serwisowa_ID,
                createUsluga_serwisowa.Data_rozpoczecia,
                createUsluga_serwisowa.ID_pracownika,
                createUsluga_serwisowa.Klient_ID,
                createUsluga_serwisowa.Cena_uslugi);
            usluga_serwisowa.Usluga_serwisowa_ID = await _usluga_serwisowaRepository.AddUsluga_serwisowa(usluga_serwisowa);
            return usluga_serwisowa;
        }

        public async Task EditUsluga_serwisowa(EditUsluga_serwisowa createUsluga_serwisowa, int Usluga_serwisowa_ID)
        {
            var usluga_serwisowa = await _usluga_serwisowaRepository.GetUslugaByID(Usluga_serwisowa_ID);
            usluga_serwisowa.EditUsluga_serwisowa(createUsluga_serwisowa.Usluga_serwisowa_ID, createUsluga_serwisowa.ID_pracownika, createUsluga_serwisowa.Cena_uslugi);
            await _usluga_serwisowaRepository.EditUsluga_serwisowa(usluga_serwisowa);
        }

        public async Task DelUslugaByID(DelUslugaByID delUslugaByID, int Usluga_serwisowa_ID)
        {
            var usluga_serwisowa = await _usluga_serwisowaRepository.GetUslugaByID(Usluga_serwisowa_ID);
            usluga_serwisowa.DelUslugaByID(Usluga_serwisowa_ID);
            await _usluga_serwisowaRepository.DelUslugaByID(usluga_serwisowa);

        }

        //Usuwanie

    }

}
